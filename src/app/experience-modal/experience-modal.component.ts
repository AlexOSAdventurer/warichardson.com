import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-experience-modal',
  templateUrl: './experience-modal.component.html',
  styleUrls: ['./experience-modal.component.css']
})
export class ExperienceModalComponent implements OnInit {
  @Input() img;
  @Input() title;
  @Input() tags;
  @Input() desc;
  @Input() link;


  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
    
  }

}
