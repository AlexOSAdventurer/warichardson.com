import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExperienceModalComponent } from './experience-modal/experience-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   name = "Alex Richardson";
   quote = "Life is about the journey, not the destination";

   current_projects = [
    {
      img: '/assets/machine-learning.png',
      title: 'Voice Transcription with Machine Learning',
      tags: 'Machine Learning, Python, Scipy, Signal Processing, Jupyter, FFT',
      desc: "This is a continuation of my final project from my Machine Learning course. I'm using Mozilla Common Voice as my dataset. I started by using FFT to generate statistics on pitch for each audio sample, and then built classifiers for gender using those statistics. Now I'm using FFT to generate spectrograms and analyze those to hopefully transcribe voice samples into text. Progress has been slow however.",
      link: "https://bitbucket.org/AlexOSAdventurer/voice-transcription"
    },
    {
      img: '/assets/uefi.png',
      title: 'UEFI Launcher Program',
      tags: 'UEFI, C, GCC, x86_64 Assembly',
      desc: "I'm working on a toy OS for self-pedagogical purposes, and the first step is writing a UEFI program. I've taken the liberty of using it as an opportunity to write a generic loader program with a menu and configuration options. It's very tricky because the only documentation that's complete and truly useful is the 2500+ page UEFI specification. I still can't help myself though!",
      link: "https://bitbucket.org/AlexOSAdventurer/uefi-bootloader"
    },
    {
      img: '/assets/drone.png',
      title: 'Computer Vision Powered Autonomous Drone Ball Shooter',
      tags: 'ROS, C++, CMake, MavLink, OpenCV',
      desc: "For my robotics class final project, we were tasked with programming drones to implement a drone game modeled after fictional Quidditch, using ROS and Mavlink with live drones. My team chose to implement a drone that could detect a ball hoop with computer vision and shoot through that hoop. My team unfortunately couldn't work on our assigned physical drone together due to COVID-19 restrictions, and had to use a simulator instead, but we were able to show a proof-of-concept. My job was programming the drone while others worked on simulator setup and modeling.",
      link: "https://bitbucket.org/AlexOSAdventurer/drone-ball-shoot"
    },
    {
      img: '/assets/radio.png',
      title: 'GNU Radio Digital Transmission Experimentation',
      tags: 'GNU Radio, C, Radio Kit usage',
      desc: "After taking a Wireless Communication course, I wanted to apply my knowledge in the real world. Using GNU radio and a personal radio kit I've experimented with end-to-end digital transmission using in-class concepts. It's surprisingly fun!",
      link: "https://bitbucket.org/AlexOSAdventurer/gnu-radio-experiment"
    }
   ];

   past_projects = [
    {
      img: '/assets/ToyCar.jpg',
      title: 'SMS-controlled Toy Car',
      desc: "For my physics electrical project, I created a toy car that's controlled by SMS. The toy car consists of a tub, four motors, breadboard, wiring, motor controller chip, and a raspberry pi. The Pi is plugged into an external phone that receives SMS. The motors I used weren't powerful enough, so the car couldn't turn well. It was still a lot of fun!",
      link: "https://bitbucket.org/AlexOSAdventurer/toycar"
    },
    {
      img: '/assets/chainloader.png',
      title: 'Legacy BIOS Chainloader Program',
      desc: 'This is a program that lets you choose which OS to boot off of a hard drive at boot-time! Written in 16 bit assembly using classic BIOS APIs.',
      link: "https://bitbucket.org/AlexOSAdventurer/alo_chainloader"
    },
    {
      img: '/assets/survey.png',
      title: "McDonald's Survey Auto-Filler",
      desc: "I created an Android app that automatically fills out McDonald's surveys and returns a redeemable promo code. You have to create a database of all the survey questions and your preselected responses. I decided on five stars for everything, and when asked about why I am satisfied, my program states: \"I was just so satisfied.\"",
      link: 'https://bitbucket.org/AlexOSAdventurer/surveyhelpertools'
    },
    {
      img: '/assets/survivalgame.png',
      title: 'Roblox Survival Game - In Progress',
      desc: "This is a half-finished survival game in Roblox from a few years ago I started. I currently have an inventory system working, along with Health and other stats.",
      link: 'https://bitbucket.org/AlexOSAdventurer/survivalgameroblox/'
    },
    {
      img: '/assets/website.png',
      title: 'This Website',
      desc: "I created this website myself. I could have just had this done professionally. However, I wanted to prove to myself that I could learn how to do this.",
      link: 'https://bitbucket.org/AlexOSAdventurer/warichardson.com' 
    }
   ];

   experiences = [
     {
      img: '/assets/UNL.jpg',
      title: 'Teaching Assistant @ University of Nebraska-Lincoln',
      desc: 'Since August 2018, I\'ve TAed, starting with the Computer Science I and II courses. Currently I am the main TA for our Programming Languages & Concepts course. Helping students is one of the most fulfilling things I do on a day-to-day basis.'
     },
     {
      img: '/assets/raikes_2.jpg',
      title: 'Raikes Design Studio Associate @ Kiewit Corporation',
      desc: 'For the 2019-2020 academic school year, my team at the Raikes Design Studio developed a prototype tracking solution for items at Kiewit construction sites. We won Gold, the second highest award for a team in the Design Studio for any given year. I handled programming the low-end hardware associated with the solution, such as wireless Qualcomm chips, and integrating it into Kiewit\'s Azure cloud infrastructure using Azure IoT. I also got to tweak and modify the build kit given with the Qualcomm SDK for better performance.' 
     },
     {
        img: '/assets/bing.png',
        title: 'Microsoft Internship @ Bing',
        desc: 'I worked as a software intern for Microsoft\'s Bing division in Seattle during the 2019 summer! I loved all of it, and got to meet amazing people. I got experience with working within a large organization, and Microsoft\'s large extensive cloud infrastructure. My work in particular was with Azure Cosmos, C#, virtual machines, and Powershell to create a data analytics tool for Bing data.'  
     },
     {
        img: '/assets/ng-logo.svg',
        title: 'Northrop Grumman Internship',
        desc: 'I worked as a software intern for Northrop Grumman\'s GAP CIE Program in Bellevue during the 2018 summer! It was an amazing experience, as I got to contribute to a real production environment, from front-end to back-end. In particular, I worked with several others to create a new microservice for classifying government documents. I learned so much - I loved it!'
     },
     {
        img: '/assets/robotics.jpeg',
        title: 'Bennington FIRST Robotics Team',
        desc: "My Junior year of high school, I started a robotics team for my local high school. I took on the role of lead programmer for the robot. Not only has it been so much fun, it's been such a unique and rich experience, because it involves working in a team towards a goal. I really like people, and I really like the people on my team."
     }
   ];

   constructor(private modalService: NgbModal) {}

   shadow_card(e) { 
     e.shadow = true;
   }

   unshadow_card(e) { 
    e.shadow = false;
   }

   select_experience(e) { 
      const modalRef = this.modalService.open(ExperienceModalComponent);
      modalRef.componentInstance.title = e.title;
      modalRef.componentInstance.img = e.img;
      modalRef.componentInstance.tags = e.tags;
      modalRef.componentInstance.desc = e.desc;
      modalRef.componentInstance.link = e.link;
   }
}
