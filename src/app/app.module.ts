import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ExperienceModalComponent } from './experience-modal/experience-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ExperienceModalComponent
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  entryComponents: [
    ExperienceModalComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
